<?php

namespace DS\AdminBundle\Controller;

use DS\AdminBundle\Utils\UserUtil;
use eZ\Bundle\EzPublishCoreBundle\Controller;

/**
 * Class BaseController
 * @package DS\AdminBundle\Controller
 */
class BaseController extends Controller
{
    /**
     * @param $type
     * @param $message
     */
    public function setFlash($type, $message)
    {
        $this->get('session')->getFlashBag()->add($type, $message);
    }
}
