<?php

namespace DS\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DS\AdminBundle\Entity\Admin;
use DS\AdminBundle\Form\AdminLoginFormType;
use DS\AdminBundle\Utils\AdminUtil;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class DefaultController
 * @package DS\AdminBundle\Controller
 * @Route("/admin")
 */
class LoginController extends BaseController
{
    /**
     * @Route("/", name="ds_admin")
     * @Template()
     */
    public function indexAction()
    {

    }

    /**
     * @Route("/login", name="ds_admin_login"))
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $admin = new Admin();
        $loginForm = $this->createForm(new AdminLoginFormType(), $admin, []);
        if ($loginForm->handleRequest($request) && $loginForm->isValid()) {
            $repo = $this->getRepository();
            $userService = $repo->getUserService();
            try {
                $loggedAdmin = $userService->loadUserByCredentials($admin->getUsername(), $admin->getPassword());
                try {
                    $repo->setCurrentUser($loggedAdmin);
                    $roles = $repo->getRoleService()->getRoleAssignmentsForUser($loggedAdmin);
                    if (!AdminUtil::isAdministrator($roles)) {
                        $token = new UsernamePasswordToken($admin->getUsername(), $admin->getPassword(), 'admin_secured_area', ['ROLE_USER']);
                        $this->container->get('security.context')->setToken($token);
                        throw new UnauthorizedException('role', 'read');
                    }
                } catch (\eZ\Publish\API\Repository\Exceptions\UnauthorizedException $e) {
                    $this->setFlash('error', 'You have no rights!');
                    return [
                        'loginForm' => $loginForm->createView()
                    ];
                }
                $token = new UsernamePasswordToken($admin->getUsername(), $admin->getPassword(), 'admin_secured_area', ['ROLE_ADMIN']);
                $this->container->get('security.context')->setToken($token);

                $this->setFlash('success', 'You are logged in');
                return $this->redirect($this->generateUrl('ds_admin_dashboard'));

            } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
                $this->setFlash('error', 'Incorrect username or password!');
            }
        }
        return [
            'loginForm' => $loginForm->createView()
        ];
    }

    /**
     * @Route("/logout", name="ds_admin_logout"))
     * @Template()
     */
    public function logoutAction()
    {
        $this->get('security.context')->setToken(null);
        $this->get('request')->getSession()->invalidate();
        $this->setFlash('success', 'You are logged out now');
        return $this->redirect($this->generateUrl('ds_admin_login'));
    }
}
