<?php
/**
 * File containing the BrowserSubContext class.
 *
 * This is the parent object of all Browser sub contexts
 *
 * @copyright Copyright (C) 1999-2014 eZ Systems AS. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU General Public License v2.0
 * @version
 */

namespace EzSystems\BehatBundle\Features\Context\Browser\SubContexts;

use Behat\Behat\Context\BehatContext;

/**
 * BrowserSubContext
 */
abstract class BrowserSubContext extends BehatContext
{

}
