<?php
/**
 * File containing the Banners Component class
 *
 * @author Thiago Campos Viana <thiagocamposviana@gmail.com>
 */

namespace Tutei\BaseBundle\Classes\Components;

use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\ContentTypeIdentifier;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\LogicalAnd;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\ParentLocationId;
use Symfony\Component\HttpFoundation\Response;
use Tutei\BaseBundle\Classes\SearchHelper;

/**
 * Renders page Banners
 */
class SimpleBanner extends Component
{

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $pathString = $this->parameters['pathString'];
        $locations = explode('/', $pathString);


        $locationId = $locations[count($locations) - 2];

        $searchService = $this->controller->getRepository()->getSearchService();

        $query = new Query();

        $query->criterion = new LogicalAnd(
            array(
                new ContentTypeIdentifier(array('simplebanner')),
                new ParentLocationId(array($locationId))
            )
        );

        $query->limit = 1;

        $repository = $this->controller->getRepository();
        $locationService = $repository->getLocationService();
        $location = $locationService->loadLocation($locationId);

        $query->sortClauses = array(
            SearchHelper::createSortClause($location)
        );
        $list = $searchService->findContent($query);


        $blocks = array();
        //dump-ul necesar :)) !!
        //\Doctrine\Common\Util\Debug::dump($list->searchHits);

        $response = new Response();
        //Return a render only if we have a simplebanner found
        if (!empty($list->searchHits[0])) {
            $un_banner = $list->searchHits[0];

            $response->setPublic();
            $response->setSharedMaxAge(86400);

            // Menu will expire when top location cache expires.
            $response->headers->set('X-Location-Id', $locationId);

            return $this->controller->render(
                'TuteiBaseBundle:parts:page_simplebanner.html.twig', array(
                'simplebanner' => $un_banner,
            ), $response
            );
        }//otherwise return a blank response (must not be null)
        else return $response;
    }

}
