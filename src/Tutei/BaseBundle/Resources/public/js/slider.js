(function ($) {
    $.fn.xpl_slider_mod = function (simple_slider_cfg) {
        var curr_el_id = this.attr('id');
        currency = this.data('currency'),
            left = this.data('long') ? -169 : -86;
        //var slider_cfg_copy=simple_slider_cfg;
        //the tooltip div html variable

        var dragger_tooltip = $('<div class="dragger-tooltip"><p><span class="dragger-tooltip-s1"></span><span>stk</span><span class="dragger-tooltip-s2"> for </span><span class="dragger-tooltip-s3"></span><span>' + currency + '</span></p></div>').css({
            position: 'absolute',
            top: 26,
            left: left
        })
        //Activate the slider
        $("#" + curr_el_id).simpleSlider(simple_slider_cfg);
        //Set the initial value
        $("#" + curr_el_id).simpleSlider("setValue", simple_slider_cfg.value);
        $("#" + curr_el_id).simpleSlider("setRatio", (simple_slider_cfg.allowedValues.indexOf(simple_slider_cfg.value) / simple_slider_cfg.allowedValues.length)); //set ratio from 0 to 1
        //Show the tooltip, with something
        $("#" + curr_el_id + "-slider").find('.dragger').html(dragger_tooltip).show();
        $("#" + curr_el_id + "-slider").find('.dragger-tooltip-s1').html(simple_slider_cfg.value);
        var priceVal = simple_slider_cfg.priceValues[simple_slider_cfg.allowedValues.indexOf(simple_slider_cfg.value)]
        $("#" + curr_el_id + "-slider").find('.dragger-tooltip-s3').html(priceVal);
        $("#buffer_emails_value").val(simple_slider_cfg.value);

        //Bind the slider:changed event
        $("#" + curr_el_id).bind("slider:changed", function (event, data) {
            var intVal = parseInt(data.value); //MANDATORY TYPECAST TO INTEGER OTHERWISE JS SUCKS (AND uses a string array)
            $("#" + curr_el_id + "-slider").find('.dragger').html(dragger_tooltip).show();
            $("#" + curr_el_id + "-slider").find('.dragger-tooltip-s1').html(intVal);
            var slider_index = simple_slider_cfg.allowedValues.indexOf(intVal.toString());
            var priceVal = simple_slider_cfg.priceValues[slider_index];
            $("#" + curr_el_id + "-slider").find('.dragger-tooltip-s3').html(priceVal);
            //Update and trigger change on #buffer_emails input value
            $("#buffer_emails_value").val(intVal);
            $("#buffer_emails_value").trigger("change");

            //Angular BufferEmails update START
            /*
             angular.injector(['ng', 'xsmtpdk']).invoke(function (XSstorage) {
             XSstorage.set('chosen_buffer_emails',data.value);
             });
             var scope = angular.element(document.getElementById("checkoutSimpleBanner")).scope();
             scope.$apply(function () {
             scope.UpdateBanner();
             });
             */
            //Angular BufferEmails update STOP

        });
    };
})(jQuery);