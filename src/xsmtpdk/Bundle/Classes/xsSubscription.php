<?php

namespace xsmtpdk\Bundle\Classes;

//Search includes;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

//
class xsSubscription
{
    public $repository;
    public $searchService;
    public $locationService;
    public $urlAliasService;
    public $contentTypeService;
    public $blankSubscription = array(
        'nodeid' => "",
        'suid' => "",
        'tag' => "",
        'title' => "",
        'description' => "",
        'extensions' => array()
    );

    public $subsc_node;
    public $SUBSC_URL = "/_XSMTPDK_/Subscriptions/";

    public function __construct(&$repository, $subscRole)
    {
        $this->repository = $repository;

        //Instantiate services;
        $this->searchService = $repository->getSearchService();
        $this->locationService = $repository->getLocationService();
        $this->urlAliasService = $repository->getURLAliasService();
        $this->contentTypeService = $repository->getContentTypeService();

        //Get type subscription folder node ID
        $this->subsc_node = $this->urlAliasService->lookup($this->SUBSC_URL . $subscRole)->destination;

        //XPL_DEBUG
        //\Doctrine\Common\Util\Debug::dump($this->subsc_node);
        //
    }

    ///////////////////////////////////////
    //Retrieves an array with available subscriptions
    public function listSubscriptions()
    {
        //Making a query of the subscriptions in the node
        $query = new Query;
        $query->criterion = new Criterion\LogicalAnd(
            array(
                //new Criterion\ContentTypeIdentifier( "folder" ),
                new Criterion\LogicalOr(array(new Criterion\ContentTypeIdentifier("xs_subscription"), new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"))),
                new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                //new Criterion\Field( "name", Criterion\Operator::EQ, $name ),
                //new Criterion\Subtree( $subscriptions_location ),
                new Criterion\ParentLocationId($this->subsc_node),
            )
        );
        $searchResult = $this->searchService->findContent($query);
        $subscriptions = array();
        foreach ($searchResult->searchHits as $elem) {
            //\Doctrine\Common\Util\Debug::dump($elem->valueObject);
            //\Doctrine\Common\Util\Debug::dump($elem->valueObject->getVersionInfo()->getContentInfo()->mainLocationId);
            //\Doctrine\Common\Util\Debug::dump($elem->valueObject->getFieldValue('title'));

            $subscriptions[] = array(
                //Info: http://pubsvn.ez.no/doxygen/5.1/NS/html/classeZ_1_1Publish_1_1API_1_1Repository_1_1Values_1_1Content_1_1ContentInfo.html
                'nodeid' => $elem->valueObject->getVersionInfo()->getContentInfo()->mainLocationId,
                'suid' => $elem->valueObject->getFieldValue('suid')->__toString(),
                'tag' => $elem->valueObject->getFieldValue('tags')->__toString(),
                'title' => $elem->valueObject->getFieldValue('title')->__toString(),
                'description' => $elem->valueObject->getFieldValue('description')->__toString(),
            );

        }
        return $subscriptions;
    }

    ///////////////////////////////////////
    //Retrieves an array with available subscriptions
    public function listSubscriptionsFull()
    {
        //Making a query of the subscriptions in the node
        $query = new Query;
        $query->criterion = new Criterion\LogicalAnd(
            array(
                //look for both subscriptions and upgrade subscriptions
                new Criterion\LogicalOr(array(new Criterion\ContentTypeIdentifier("xs_subscription"), new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"))),
                //new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                new Criterion\ParentLocationId($this->subsc_node),
            )
        );
        $searchResult = $this->searchService->findContent($query);
        $subscriptions = array();
        foreach ($searchResult->searchHits as $elem) {
            $sc_suid = $elem->valueObject->getFieldValue('suid')->__toString();
            $subscriptions[] = $this->getSubscription($sc_suid);
            //\Doctrine\Common\Util\Debug::dump($sc_suid);
        }
        return $subscriptions;
    }

    ///////////////////////////////////////
    //Retrieves an array with available subscriptions (primary elements of the array returned are the tag names)
    public function listSubscriptionsTaggedFull($properties = array())
    {
        $prop_tagindex = false;
        if ($properties['TagOrderIndex'] == true) $prop_tagindex = true;
        //Making a query of the subscriptions in the node
        $query = new Query;
        $query->criterion = new Criterion\LogicalAnd(
            array(
                //look for both subscriptions and upgrade subscriptions
                new Criterion\LogicalOr(array(new Criterion\ContentTypeIdentifier("xs_subscription"), new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"))),
                //new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                new Criterion\ParentLocationId($this->subsc_node),
            )
        );
        //Priority Sort Order (Ascending)
        $query->sortClauses = array(new SortClause\LocationPriority(Query::SORT_ASC));

        $searchResult = $this->searchService->findContent($query);
        $subscriptions = array();
        foreach ($searchResult->searchHits as $elem) {
            $sc_suid = $elem->valueObject->getFieldValue('suid')->__toString();
            $sc = $this->getSubscription($sc_suid);
            //If the subscription tag is empty, by default call it MISC
            if (!strlen($sc['tag'])) $sc['tag'] = 'MISC';
            //Initialise the subscription tag name subarray
            if (!isset($subscriptions[$sc['tag']])) $subscriptions[$sc['tag']] = array();
            //Place the subscription as an subelement of the current tag
            $subscriptions[$sc['tag']][] = $this->getSubscription($sc_suid);
            //\Doctrine\Common\Util\Debug::dump($sc_suid);
        }
        //Create the tag order array (for angular)
        if ($prop_tagindex) {
            $subscriptions = array(
                'tags_data' => $subscriptions,
                'tags_order' => array());
            foreach ($subscriptions['tags_data'] as $tag_name => $value) {
                $subscriptions['tags_order'][] = $tag_name;
            }
        }
        return $subscriptions;
    }
    ///////////////////////////////////////
    //Retrieves a subscription based on suid
    public function getSubscription($suid)
    {
        //Making a single search for the subscription with the requested suid in the subscriptions node
        $crit = new Criterion\LogicalAnd(
            array(
                //new Criterion\ContentTypeIdentifier( "xs_subscription" ),
                new Criterion\LogicalOr(array(new Criterion\ContentTypeIdentifier("xs_subscription"), new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"))),
                new Criterion\Field("suid", Criterion\Operator::EQ, $suid),
                new Criterion\ParentLocationId($this->subsc_node),
            )
        );

        //try catch
        // one catch is enough
        try {
            $oneResult = $this->searchService->findSingle($crit);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return false;
        } catch (\eZ\Publish\API\Repository\Exceptions\InvalidArgumentException $e) {
            return false;
        }

        $aSubscription = array(
            //Info: http://pubsvn.ez.no/doxygen/5.1/NS/html/classeZ_1_1Publish_1_1API_1_1Repository_1_1Values_1_1Content_1_1ContentInfo.html
            'nodeid' => $oneResult->getVersionInfo()->getContentInfo()->mainLocationId,
            'suid' => $oneResult->getFieldValue('suid')->__toString(),
            'tag' => $oneResult->getFieldValue('tag')->__toString(),
            'title' => $oneResult->getFieldValue('title')->__toString(),
            'description' => $oneResult->getFieldValue('description')->__toString(),
            'extensions' => array(),
        );

        $query = new Query;
        $query->criterion = new Criterion\ParentLocationId($aSubscription['nodeid']);
        $searchResult = $this->searchService->findContent($query);
        foreach ($searchResult->searchHits as $extension) {
            //\Doctrine\Common\Util\Debug::dump($subelem->valueObject);
            $extension_contentTypeId = $extension->valueObject->getVersionInfo()->getContentInfo()->contentTypeId;
            $contentType = $this->contentTypeService->loadContentType($extension_contentTypeId);

            //retrieving the string class identifier of each subscription extension
            //info:  http://share.ez.no/forums/ez-publish-5-platform/how-to-retrieve-the-contenttype-identifier-aka-class_identifier
            $extension_identifier = $contentType->identifier;


            // Preload the previous value.
            if (isset($aSubscription['extensions'][$extension_identifier])) $extension_val = $aSubscription['extensions'][$extension_identifier];
            else $extension_val = false;

            switch ($extension_identifier) {
                case "xs_email_relays"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('no_relays')->__toString();
                    $extension_val += $tmp_val;
                    //apply a minimum correction
                    if ($extension_val < 0) $extension_val = 0;
                    break;

                case "xs_daily_included_emails"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('daily_top')->__toString();
                    $extension_val += $tmp_val;
                    //apply a minimum correction
                    if ($extension_val < 0) $extension_val = 0;
                    break;

                case "xs_shared_daily_limit"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('daily_limit')->__toString();
                    $extension_val += $tmp_val;
                    //apply a minimum correction
                    if ($extension_val < 0) $extension_val = 0;
                    break;

                case "xs_price_tag"    : //cumulative
                    $tmp_price_dkk = (int)$extension->valueObject->getFieldValue('dkk')->__toString();
                    $tmp_price_eur = (int)$extension->valueObject->getFieldValue('eur')->__toString();

                    //Initialise the val with a blank array if no previous value was found.
                    if (!$extension_val) $extension_val = array('dkk' => 0, 'eur' => 0);

                    //now add  them to the previous values
                    $extension_val['dkk'] += $tmp_price_dkk;
                    $extension_val['eur'] += $tmp_price_eur;

                    break;

                case "xs_discount"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('percent')->__toString();
                    $extension_val += $tmp_val;

                    //Apply correction for min-max percentage
                    if ($extension_val > 100) $extension_val = 100;
                    if ($extension_val < 0) $extension_val = 0;
                    break;

                case "xs_expiry"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('days')->__toString();
                    $extension_val += $tmp_val;

                    //Apply correction for min-max
                    if ($extension_val < 0) $extension_val = 0;
                    break;

                case "xs_smtp_sendas_no"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('bulk_no')->__toString();
                    $extension_val += $tmp_val;
                    //apply a minimum correction
                    if ($extension_val < 1) $extension_val = 0;
                    break;

                case "xs_smtp_acounts_no"    : //cumulative
                    if (!$extension_val) $extension_val = 0;
                    $tmp_val = $extension->valueObject->getFieldValue('bulk_no')->__toString();
                    $extension_val += $tmp_val;
                    //apply a minimum correction
                    if ($extension_val < 1) $extension_val = 0;
                    break;
                case "xs_tooltip":
                    $extension_val = $extension->valueObject->getFieldValue('tooltip')->__toString();
                    break;

            }
            $aSubscription['extensions'][$extension_identifier] = $extension_val;

        }
        return $aSubscription;
    }
    //////////////////////////////////////////////////////////////////////////////
    // Create the compound subcscription from 2 different subscriptions, and cumulate the extensions

    public function getCompoundSubscription($base_type, $base_suid, $upgrade_type, $upgrade_suid)
    {
        $baseSubsc_one = $this->getSubscription($base_suid);
        $original_subsc_node = $this->subsc_node;
        $this->subsc_node = $this->urlAliasService->lookup($this->SUBSC_URL . $upgrade_type)->destination;
        $upgradeSubsc_one = $this->getSubscription($upgrade_suid);

        //Initialise with empty arrays if they were not found
        if (is_bool($baseSubsc_one)) $baseSubsc_one = $this->blankSubscription;
        if (is_bool($upgradeSubsc_one)) $upgradeSubsc_one = $this->blankSubscription;

        $compoundSubsc = array();
        $compoundSubsc['base_suid'] = $base_suid;
        $compoundSubsc['upgrade_suid'] = $upgrade_suid;
        $compoundSubsc['title'] = $baseSubsc_one['title'] . ' + ' . $upgradeSubsc_one['title'];
        $compoundSubsc['base_title'] = $baseSubsc_one['title'];
        $compoundSubsc['upgrade_title'] = $upgradeSubsc_one['title'];
        $compoundSubsc['base_description'] = $baseSubsc_one['description'];
        $compoundSubsc['upgrade_description'] = $upgradeSubsc_one['description'];
        $compoundSubsc['base_tag'] = $baseSubsc_one['tag'];
        $compoundSubsc['upgrade_tag'] = $upgradeSubsc_one['tag'];
        $compoundSubsc['extensions'] = array();

        foreach (array($baseSubsc_one, $upgradeSubsc_one) as $aSubscription)
            foreach ($aSubscription['extensions'] as $extension_identifier => $value) {
                switch ($extension_identifier) {
                    case "xs_price_tag":
                        if (!isset($compoundSubsc['extensions']['xs_price_tag'])) $compoundSubsc['extensions'][$extension_identifier] = $value;
                        else {
                            $compoundSubsc['extensions']['xs_price_tag']['dkk'] += $value['dkk'];
                            $compoundSubsc['extensions']['xs_price_tag']['eur'] += $value['eur'];
                        }
                        break;
                    default:
                        if (!isset($compoundSubsc['extensions'][$extension_identifier])) $compoundSubsc['extensions'][$extension_identifier] = $value;
                        else {
                            $compoundSubsc['extensions'][$extension_identifier] += $value;
                        }
                }
            }
        return $compoundSubsc;
    }
    ///////////////////////////////////////
    //Retrieves an array of upgrade subscriptions that have a relation to the $base_suid (base subscription)
    public function getSubscriptionUpgrades($base_type, $base_suid, $upgrade_type)
    {
        //Get base_type subscription folder node ID
        $baseSubscFolderNode = $this->urlAliasService->lookup($this->SUBSC_URL . $base_type)->destination;
        $this->subsc_node = $this->urlAliasService->lookup($this->SUBSC_URL . $upgrade_type)->destination;
        //Making a single search for the base subscription with the requested base_suid in the subscriptions node folder
        $crit = new Criterion\LogicalAnd(
            array(
                //new Criterion\ContentTypeIdentifier( "xs_subscription" ),
                new Criterion\LogicalOr(array(new Criterion\ContentTypeIdentifier("xs_subscription"), new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"))),
                new Criterion\Field("suid", Criterion\Operator::EQ, $base_suid),
                new Criterion\ParentLocationId($baseSubscFolderNode),
            )
        );

        //try catch
        // one catch is enough
        try {
            $oneResult = $this->searchService->findSingle($crit);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return false;
        } catch (\eZ\Publish\API\Repository\Exceptions\InvalidArgumentException $e) {
            return false;
        }

        //get the Cid of the base subscription
        $baseSubscCid = $oneResult->getVersionInfo()->getContentInfo()->id;
        //\Doctrine\Common\Util\Debug::dump($baseSubscCid);


        //Making a query of the subscriptions in the $upgrade_type_node (previously set in $this->subsc_node
        $query = new Query;
        $query->criterion = new Criterion\LogicalAnd(
            array(
                //look for both subscriptions and upgrade subscriptions
                new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"),
                //new Criterion\Visibility( Criterion\Visibility::VISIBLE ),
                new Criterion\ParentLocationId($this->subsc_node),
            )
        );
        $searchResult = $this->searchService->findContent($query);
        $subscriptions = array();
        foreach ($searchResult->searchHits as $elem) {
            //Get the relation array
            $elemRelationList = $elem->valueObject->getFieldValue('available_only_for_subscription')->destinationContentIds;
            //Get info only for upgrade subscriptions that have the base subscription Cid in the relation list
            if (in_array($baseSubscCid, $elemRelationList)) {
                $sc_suid = $elem->valueObject->getFieldValue('suid')->__toString();
                $subscriptions[] = $this->getSubscription($sc_suid);
                //\Doctrine\Common\Util\Debug::dump($sc_suid);
            }
        }
        return $subscriptions;

    }

    //////////////////////////////////////////////////////////////////
    public function copySubscription($suid, $ParentLocationId)
    {
        //Making a single search for the subscription with the requested suid in the subscriptions node
        $crit = new Criterion\LogicalAnd(
            array(
                //new Criterion\ContentTypeIdentifier( "xs_subscription" ),
                new Criterion\LogicalOr(array(new Criterion\ContentTypeIdentifier("xs_subscription"), new Criterion\ContentTypeIdentifier("xs_subscription_upgrade"))),
                new Criterion\Field("suid", Criterion\Operator::EQ, $suid),
                new Criterion\ParentLocationId($this->subsc_node),
            )
        );

        //try catch
        try {
            $oneResult = $this->searchService->findSingle($crit);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return false;
        } catch (\eZ\Publish\API\Repository\Exceptions\InvalidArgumentException $e) {
            return false;
        }

        $sNodeID = $oneResult->getVersionInfo()->getContentInfo()->mainLocationId;


        // Load location to copy
        $locationToCopy = $this->locationService->loadLocation($sNodeID);
        // Load new parent location
        $newParentLocation = $this->locationService->loadLocation($ParentLocationId);

        //Copy the subscription
        $locationService = $this->locationService;

        //\Doctrine\Common\Util\Debug::dump("Children: ". $locationService->getLocationChildCount($newParentLocation));

        //execute copySubtree under sudo
        $copiedLocation = $this->repository->sudo(
            function ($locationService) use ($locationService, $locationToCopy, $newParentLocation) {
                $locationService->copySubtree($locationToCopy, $newParentLocation);
            }
        );

        //removing targeted legacy cache for the $ParentLocationId
        $this->removeNodeCache($ParentLocationId);
    }

    /////////////////////////////////////////
    public function removeNodeCache($nodeID)
    {
        // string to search in a filename.
        $searchString = 'Node ID: ' . $nodeID;
        //Retrieving the ez5 folder path from env
        $ez5_dir = dirname(dirname($_SERVER['SCRIPT_FILENAME']));
        //The legacy folder path
        $searchDir = $ez5_dir . '/ezpublish_legacy/var/cache/content/siteadmin/';
        //Grep recursively for the files containing the Node ID: string
        $result = shell_exec('grep --files-with-matches -Ri "' . $searchString . '" ' . $searchDir);
        $result = explode("\n", $result);
        $filesRemoved = array();
        //removing the files
        foreach ($result as $cacheFile) {
            if (strlen($cacheFile) > strlen($searchDir)) unlink($cacheFile);
            $filesRemoved[] = $cacheFile;
        }
        return $filesRemoved;
    }

}
