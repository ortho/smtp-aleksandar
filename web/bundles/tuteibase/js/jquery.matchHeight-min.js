



<!DOCTYPE html>
<html lang="en" class="   ">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    
    
    <title>jquery-match-height/jquery.matchHeight-min.js at master · liabru/jquery-match-height · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="liabru/jquery-match-height" name="twitter:title" /><meta content="jquery-match-height - a robust, responsive equal heights plugin for jQuery" name="twitter:description" /><meta content="https://avatars3.githubusercontent.com/u/654420?v=2&amp;s=400" name="twitter:image:src" />
<meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars3.githubusercontent.com/u/654420?v=2&amp;s=400" property="og:image" /><meta content="liabru/jquery-match-height" property="og:title" /><meta content="https://github.com/liabru/jquery-match-height" property="og:url" /><meta content="jquery-match-height - a robust, responsive equal heights plugin for jQuery" property="og:description" />

    <link rel="assets" href="https://assets-cdn.github.com/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035">
    

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>
      <meta name="google-analytics" content="UA-3769691-2">

    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="567D3E7D:4A6D:1903DF0C:53F4C917" name="octolytics-dimension-request_id" />
    

    
    
    <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">


    <meta content="authenticity_token" name="csrf-param" />
<meta content="QRqTsB9fXsM04JgDw1+Us/wSD7w3Q+bP9hA7PsH3vevDpwRMNBNxL3CI0uR9W8Ch3uBvxWbf53Yfbu6KRBRi4w==" name="csrf-token" />

    <link href="https://assets-cdn.github.com/assets/github-1cb8b113a5171f42a80f0c4ae25f960189bea016.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://assets-cdn.github.com/assets/github2-b695851dc3dd961ba4f37278b1ce768146fcbdc1.css" media="all" rel="stylesheet" type="text/css" />
    


    <meta http-equiv="x-pjax-version" content="846d805999bddb6634128596bde12994">

      
  <meta name="description" content="jquery-match-height - a robust, responsive equal heights plugin for jQuery">


  <meta content="654420" name="octolytics-dimension-user_id" /><meta content="liabru" name="octolytics-dimension-user_login" /><meta content="17341429" name="octolytics-dimension-repository_id" /><meta content="liabru/jquery-match-height" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="17341429" name="octolytics-dimension-repository_network_root_id" /><meta content="liabru/jquery-match-height" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/liabru/jquery-match-height/commits/master.atom" rel="alternate" title="Recent Commits to jquery-match-height:master" type="application/atom+xml">

  </head>


  <body class="logged_out  env-production windows vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      
      


      
      <div class="header header-logged-out">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions">
        <a class="button primary" href="/join">Sign up</a>
      <a class="button signin" href="/login?return_to=%2Fliabru%2Fjquery-match-height%2Fblob%2Fmaster%2Fjquery.matchHeight-min.js">Sign in</a>
    </div>

    <div class="site-search repo-scope js-site-search">
      <form accept-charset="UTF-8" action="/liabru/jquery-match-height/search" class="js-site-search-form" data-global-search-url="/search" data-repo-search-url="/liabru/jquery-match-height/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
  <input type="text"
    class="js-site-search-field is-clearable"
    data-hotkey="s"
    name="q"
    placeholder="Search"
    data-global-scope-placeholder="Search GitHub"
    data-repo-scope-placeholder="Search"
    tabindex="1"
    autocapitalize="off">
  <div class="scope-badge">This repository</div>
</form>
    </div>

      <ul class="header-nav left">
          <li class="header-nav-item">
            <a class="header-nav-link" href="/explore">Explore</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/features">Features</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="https://enterprise.github.com/">Enterprise</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/blog">Blog</a>
          </li>
      </ul>

  </div>
</div>



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    <div id="js-flash-container">
      
    </div>
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        
<ul class="pagehead-actions">


  <li>
      <a href="/login?return_to=%2Fliabru%2Fjquery-match-height"
    class="minibutton with-count star-button tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <span class="octicon octicon-star"></span>
    Star
  </a>

    <a class="social-count js-social-count" href="/liabru/jquery-match-height/stargazers">
      555
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2Fliabru%2Fjquery-match-height"
        class="minibutton with-count js-toggler-target fork-button tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-repo-forked"></span>
        Fork
      </a>
      <a href="/liabru/jquery-match-height/network" class="social-count">
        90
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="mega-octicon octicon-repo"></span>
          <span class="author"><a href="/liabru" class="url fn" itemprop="url" rel="author"><span itemprop="title">liabru</span></a></span><!--
       --><span class="path-divider">/</span><!--
       --><strong><a href="/liabru/jquery-match-height" class="js-current-repository js-repo-home-link">jquery-match-height</a></strong>

          <span class="page-context-loader">
            <img alt="" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">
      <div class="repository-with-sidebar repo-container new-discussion-timeline  ">
        <div class="repository-sidebar clearfix">
            
<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders" data-issue-count-url="/liabru/jquery-match-height/issues/counts">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped tooltipped-w" aria-label="Code">
        <a href="/liabru/jquery-match-height" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-hotkey="g c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /liabru/jquery-match-height">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

        <li class="tooltipped tooltipped-w" aria-label="Issues">
          <a href="/liabru/jquery-match-height/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /liabru/jquery-match-height/issues">
            <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
            <span class="js-issue-replace-counter"></span>
            <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>

      <li class="tooltipped tooltipped-w" aria-label="Pull Requests">
        <a href="/liabru/jquery-match-height/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-hotkey="g p" data-selected-links="repo_pulls /liabru/jquery-match-height/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class="js-pull-replace-counter"></span>
            <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped tooltipped-w" aria-label="Pulse">
        <a href="/liabru/jquery-match-height/pulse/weekly" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /liabru/jquery-match-height/pulse/weekly">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Graphs">
        <a href="/liabru/jquery-match-height/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /liabru/jquery-match-height/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

              <div class="only-with-full-nav">
                
  
<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="input-group">
    <input type="text" class="input-mini input-monospace js-url-field"
           value="https://github.com/liabru/jquery-match-height.git" readonly="readonly">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="https://github.com/liabru/jquery-match-height.git" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>

  
<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="input-group">
    <input type="text" class="input-mini input-monospace js-url-field"
           value="https://github.com/liabru/jquery-match-height" readonly="readonly">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="https://github.com/liabru/jquery-match-height" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <a href="https://help.github.com/articles/which-remote-url-should-i-use" class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
    <span class="octicon octicon-question"></span>
  </a>
</p>


  <a href="http://windows.github.com" class="minibutton sidebar-button" title="Save liabru/jquery-match-height to your computer and use it in GitHub Desktop." aria-label="Save liabru/jquery-match-height to your computer and use it in GitHub Desktop.">
    <span class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

                <a href="/liabru/jquery-match-height/archive/master.zip"
                   class="minibutton sidebar-button"
                   aria-label="Download liabru/jquery-match-height as a zip file"
                   title="Download liabru/jquery-match-height as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          

<a href="/liabru/jquery-match-height/blob/9ba952912342ff14fb81804a8f7967e37679c385/jquery.matchHeight-min.js" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:22bc5b8e8f2d6f50a00d0af9fff9e328 -->

<div class="file-navigation">
  
<div class="select-menu js-menu-container js-select-menu left">
  <span class="minibutton select-menu-button js-menu-target css-truncate" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    title="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-x js-menu-close" role="button" aria-label="Close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/liabru/jquery-match-height/blob/master/jquery.matchHeight-min.js"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/liabru/jquery-match-height/tree/0.5.2/jquery.matchHeight-min.js"
                 data-name="0.5.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="0.5.2">0.5.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/liabru/jquery-match-height/tree/0.5.1/jquery.matchHeight-min.js"
                 data-name="0.5.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="0.5.1">0.5.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/liabru/jquery-match-height/tree/0.5.0/jquery.matchHeight-min.js"
                 data-name="0.5.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="0.5.0">0.5.0</a>
            </div> <!-- /.select-menu-item -->
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="button-group right">
    <a href="/liabru/jquery-match-height/find/master"
          class="js-show-file-finder minibutton empty-icon tooltipped tooltipped-s"
          data-pjax
          data-hotkey="t"
          aria-label="Quickly jump between files">
      <span class="octicon octicon-list-unordered"></span>
    </a>
    <button class="js-zeroclipboard minibutton zeroclipboard-button"
          data-clipboard-text="jquery.matchHeight-min.js"
          aria-label="Copy to clipboard"
          data-copied-hint="Copied!">
      <span class="octicon octicon-clippy"></span>
    </button>
  </div>

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/liabru/jquery-match-height" class="" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">jquery-match-height</span></a></span></span><span class="separator"> / </span><strong class="final-path">jquery.matchHeight-min.js</strong>
  </div>
</div>


  <div class="commit file-history-tease">
      <img alt="Liam Brummitt" class="main-avatar" data-user="654420" height="24" src="https://avatars0.githubusercontent.com/u/654420?v=2&amp;s=48" width="24" />
      <span class="author"><a href="/liabru" rel="author">liabru</a></span>
      <time datetime="2014-08-18T19:39:47+01:00" is="relative-time">August 18, 2014</time>
      <div class="commit-title">
          <a href="/liabru/jquery-match-height/commit/ee83317ea0a231ba869340dfcc38f9565956c152" class="message" data-pjax="true" title="added maintainScroll functionality, closes #18">added maintainScroll functionality, closes</a> <a href="https://github.com/liabru/jquery-match-height/issues/18" class="issue-link" title="Page jumps up on reloading the elements when using ajax pagination">#18</a>
      </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>3</strong>  contributors</a></p>
      
    <a class="avatar tooltipped tooltipped-s" aria-label="liabru" href="/liabru/jquery-match-height/commits/master/jquery.matchHeight-min.js?author=liabru"><img alt="Liam Brummitt" data-user="654420" height="20" src="https://avatars2.githubusercontent.com/u/654420?v=2&amp;s=40" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="stefanozoffoli" href="/liabru/jquery-match-height/commits/master/jquery.matchHeight-min.js?author=stefanozoffoli"><img alt="Stefano Zoffoli" data-user="1004488" height="20" src="https://avatars3.githubusercontent.com/u/1004488?v=2&amp;s=40" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="dcorb" href="/liabru/jquery-match-height/commits/master/jquery.matchHeight-min.js?author=dcorb"><img alt="Corbacho" data-user="881333" height="20" src="https://avatars3.githubusercontent.com/u/881333?v=2&amp;s=40" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="Liam Brummitt" data-user="654420" height="24" src="https://avatars0.githubusercontent.com/u/654420?v=2&amp;s=48" width="24" />
            <a href="/liabru">liabru</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Stefano Zoffoli" data-user="1004488" height="24" src="https://avatars1.githubusercontent.com/u/1004488?v=2&amp;s=48" width="24" />
            <a href="/stefanozoffoli">stefanozoffoli</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Corbacho" data-user="881333" height="24" src="https://avatars1.githubusercontent.com/u/881333?v=2&amp;s=48" width="24" />
            <a href="/dcorb">dcorb</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file-box">
  <div class="file">
    <div class="meta clearfix">
      <div class="info file-name">
          <span>10 lines (10 sloc)</span>
          <span class="meta-divider"></span>
        <span>2.514 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
          <a href="/liabru/jquery-match-height/raw/master/jquery.matchHeight-min.js" class="minibutton " id="raw-url">Raw</a>
            <a href="/liabru/jquery-match-height/blame/master/jquery.matchHeight-min.js" class="minibutton js-update-url-with-hash">Blame</a>
          <a href="/liabru/jquery-match-height/commits/master/jquery.matchHeight-min.js" class="minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->

          <a class="octicon-button tooltipped tooltipped-nw"
             href="http://windows.github.com" aria-label="Open this file in GitHub for Windows">
              <span class="octicon octicon-device-desktop"></span>
          </a>

            <a class="octicon-button disabled tooltipped tooltipped-w" href="#"
               aria-label="You must be signed in to make or propose changes"><span class="octicon octicon-pencil"></span></a>

          <a class="octicon-button danger disabled tooltipped tooltipped-w" href="#"
             aria-label="You must be signed in to make or propose changes">
          <span class="octicon octicon-trashcan"></span>
        </a>
      </div><!-- /.actions -->
    </div>
      
  <div class="blob-wrapper data type-javascript">
      <table class="highlight tab-size-8 js-file-line-container">
      <tr>
        <td id="L1" class="blob-line-num js-line-number" data-line-number="1"></td>
        <td id="LC1" class="blob-line-code js-file-line"><span class="cm">/**</span></td>
      </tr>
      <tr>
        <td id="L2" class="blob-line-num js-line-number" data-line-number="2"></td>
        <td id="LC2" class="blob-line-code js-file-line"><span class="cm">* jquery.matchHeight-min.js v0.5.2</span></td>
      </tr>
      <tr>
        <td id="L3" class="blob-line-num js-line-number" data-line-number="3"></td>
        <td id="LC3" class="blob-line-code js-file-line"><span class="cm">* http://brm.io/jquery-match-height/</span></td>
      </tr>
      <tr>
        <td id="L4" class="blob-line-num js-line-number" data-line-number="4"></td>
        <td id="LC4" class="blob-line-code js-file-line"><span class="cm">* License: MIT</span></td>
      </tr>
      <tr>
        <td id="L5" class="blob-line-num js-line-number" data-line-number="5"></td>
        <td id="LC5" class="blob-line-code js-file-line"><span class="cm">*/</span></td>
      </tr>
      <tr>
        <td id="L6" class="blob-line-num js-line-number" data-line-number="6"></td>
        <td id="LC6" class="blob-line-code js-file-line"><span class="p">(</span><span class="kd">function</span><span class="p">(</span><span class="nx">b</span><span class="p">){</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="o">=</span><span class="kd">function</span><span class="p">(</span><span class="nx">a</span><span class="p">){</span><span class="k">if</span><span class="p">(</span><span class="s2">&quot;remove&quot;</span><span class="o">===</span><span class="nx">a</span><span class="p">){</span><span class="kd">var</span> <span class="nx">d</span><span class="o">=</span><span class="k">this</span><span class="p">;</span><span class="k">this</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;height&quot;</span><span class="p">,</span><span class="s2">&quot;&quot;</span><span class="p">);</span><span class="nx">b</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_groups</span><span class="p">,</span><span class="kd">function</span><span class="p">(</span><span class="nx">b</span><span class="p">,</span><span class="nx">a</span><span class="p">){</span><span class="nx">a</span><span class="p">.</span><span class="nx">elements</span><span class="o">=</span><span class="nx">a</span><span class="p">.</span><span class="nx">elements</span><span class="p">.</span><span class="nx">not</span><span class="p">(</span><span class="nx">d</span><span class="p">)});</span><span class="k">return</span> <span class="k">this</span><span class="p">}</span><span class="k">if</span><span class="p">(</span><span class="mi">1</span><span class="o">&gt;=</span><span class="k">this</span><span class="p">.</span><span class="nx">length</span><span class="p">)</span><span class="k">return</span> <span class="k">this</span><span class="p">;</span><span class="nx">a</span><span class="o">=</span><span class="s2">&quot;undefined&quot;</span><span class="o">!==</span><span class="k">typeof</span> <span class="nx">a</span><span class="o">?</span><span class="nx">a</span><span class="o">:!</span><span class="mi">0</span><span class="p">;</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_groups</span><span class="p">.</span><span class="nx">push</span><span class="p">({</span><span class="nx">elements</span><span class="o">:</span><span class="k">this</span><span class="p">,</span><span class="nx">byRow</span><span class="o">:</span><span class="nx">a</span><span class="p">});</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_apply</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="nx">a</span><span class="p">);</span><span class="k">return</span> <span class="k">this</span><span class="p">};</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_apply</span><span class="o">=</span><span class="kd">function</span><span class="p">(</span><span class="nx">a</span><span class="p">,</span><span class="nx">d</span><span class="p">){</span><span class="kd">var</span> <span class="nx">c</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="nx">a</span><span class="p">),</span><span class="nx">f</span><span class="o">=</span><span class="p">[</span><span class="nx">c</span><span class="p">],</span><span class="nx">e</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="nb">window</span><span class="p">).</span><span class="nx">scrollTop</span><span class="p">(),</span><span class="nx">h</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="s2">&quot;html&quot;</span><span class="p">).</span><span class="nx">outerHeight</span><span class="p">(</span><span class="o">!</span><span class="mi">0</span><span class="p">);</span><span class="nx">d</span><span class="o">&amp;&amp;</span><span class="p">(</span><span class="nx">c</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">(){</span><span class="kd">var</span> <span class="nx">a</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="k">this</span><span class="p">),</span><span class="nx">c</span><span class="o">=</span><span class="s2">&quot;inline-block&quot;</span><span class="o">===</span></td>
      </tr>
      <tr>
        <td id="L7" class="blob-line-num js-line-number" data-line-number="7"></td>
        <td id="LC7" class="blob-line-code js-file-line"><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;display&quot;</span><span class="p">)</span><span class="o">?</span><span class="s2">&quot;inline-block&quot;</span><span class="o">:</span><span class="s2">&quot;block&quot;</span><span class="p">;</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">({</span><span class="nx">display</span><span class="o">:</span><span class="nx">c</span><span class="p">,</span><span class="s2">&quot;padding-top&quot;</span><span class="o">:</span><span class="s2">&quot;0&quot;</span><span class="p">,</span><span class="s2">&quot;padding-bottom&quot;</span><span class="o">:</span><span class="s2">&quot;0&quot;</span><span class="p">,</span><span class="s2">&quot;border-top-width&quot;</span><span class="o">:</span><span class="s2">&quot;0&quot;</span><span class="p">,</span><span class="s2">&quot;border-bottom-width&quot;</span><span class="o">:</span><span class="s2">&quot;0&quot;</span><span class="p">,</span><span class="nx">height</span><span class="o">:</span><span class="s2">&quot;100px&quot;</span><span class="p">})}),</span><span class="nx">f</span><span class="o">=</span><span class="nx">m</span><span class="p">(</span><span class="nx">c</span><span class="p">),</span><span class="nx">c</span><span class="p">.</span><span class="nx">css</span><span class="p">({</span><span class="nx">display</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">,</span><span class="s2">&quot;padding-top&quot;</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">,</span><span class="s2">&quot;padding-bottom&quot;</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">,</span><span class="s2">&quot;border-top-width&quot;</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">,</span><span class="s2">&quot;border-bottom-width&quot;</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">,</span><span class="nx">height</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">}));</span><span class="nx">b</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="nx">f</span><span class="p">,</span><span class="kd">function</span><span class="p">(</span><span class="nx">a</span><span class="p">,</span><span class="nx">c</span><span class="p">){</span><span class="kd">var</span> <span class="nx">d</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="nx">c</span><span class="p">),</span><span class="nx">f</span><span class="o">=</span><span class="mi">0</span><span class="p">,</span><span class="nx">e</span><span class="o">=</span><span class="nx">d</span><span class="p">.</span><span class="nx">parents</span><span class="p">().</span><span class="nx">add</span><span class="p">(</span><span class="nx">d</span><span class="p">).</span><span class="nx">filter</span><span class="p">(</span><span class="s2">&quot;:hidden&quot;</span><span class="p">);</span><span class="nx">e</span><span class="p">.</span><span class="nx">css</span><span class="p">({</span><span class="nx">display</span><span class="o">:</span><span class="s2">&quot;block&quot;</span><span class="p">});</span><span class="nx">d</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">(){</span><span class="kd">var</span> <span class="nx">a</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="k">this</span><span class="p">),</span><span class="nx">c</span><span class="o">=</span><span class="s2">&quot;inline-block&quot;</span><span class="o">===</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;display&quot;</span><span class="p">)</span><span class="o">?</span><span class="s2">&quot;inline-block&quot;</span><span class="o">:</span><span class="s2">&quot;block&quot;</span><span class="p">;</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">({</span><span class="nx">display</span><span class="o">:</span><span class="nx">c</span><span class="p">,</span><span class="nx">height</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">});</span></td>
      </tr>
      <tr>
        <td id="L8" class="blob-line-num js-line-number" data-line-number="8"></td>
        <td id="LC8" class="blob-line-code js-file-line"><span class="nx">a</span><span class="p">.</span><span class="nx">outerHeight</span><span class="p">(</span><span class="o">!</span><span class="mi">1</span><span class="p">)</span><span class="o">&gt;</span><span class="nx">f</span><span class="o">&amp;&amp;</span><span class="p">(</span><span class="nx">f</span><span class="o">=</span><span class="nx">a</span><span class="p">.</span><span class="nx">outerHeight</span><span class="p">(</span><span class="o">!</span><span class="mi">1</span><span class="p">));</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">({</span><span class="nx">display</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">})});</span><span class="nx">e</span><span class="p">.</span><span class="nx">css</span><span class="p">({</span><span class="nx">display</span><span class="o">:</span><span class="s2">&quot;&quot;</span><span class="p">});</span><span class="nx">d</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">(){</span><span class="kd">var</span> <span class="nx">a</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="k">this</span><span class="p">),</span><span class="nx">c</span><span class="o">=</span><span class="mi">0</span><span class="p">;</span><span class="s2">&quot;border-box&quot;</span><span class="o">!==</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;box-sizing&quot;</span><span class="p">)</span><span class="o">&amp;&amp;</span><span class="p">(</span><span class="nx">c</span><span class="o">+=</span><span class="nx">g</span><span class="p">(</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;border-top-width&quot;</span><span class="p">))</span><span class="o">+</span><span class="nx">g</span><span class="p">(</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;border-bottom-width&quot;</span><span class="p">)),</span><span class="nx">c</span><span class="o">+=</span><span class="nx">g</span><span class="p">(</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;padding-top&quot;</span><span class="p">))</span><span class="o">+</span><span class="nx">g</span><span class="p">(</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;padding-bottom&quot;</span><span class="p">)));</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;height&quot;</span><span class="p">,</span><span class="nx">f</span><span class="o">-</span><span class="nx">c</span><span class="p">)})});</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_maintainScroll</span><span class="o">&amp;&amp;</span><span class="nx">b</span><span class="p">(</span><span class="nb">window</span><span class="p">).</span><span class="nx">scrollTop</span><span class="p">(</span><span class="nx">e</span><span class="o">/</span><span class="nx">h</span><span class="o">*</span><span class="nx">b</span><span class="p">(</span><span class="s2">&quot;html&quot;</span><span class="p">).</span><span class="nx">outerHeight</span><span class="p">(</span><span class="o">!</span><span class="mi">0</span><span class="p">));</span><span class="k">return</span> <span class="k">this</span><span class="p">};</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_applyDataApi</span><span class="o">=</span><span class="kd">function</span><span class="p">(){</span><span class="kd">var</span> <span class="nx">a</span><span class="o">=</span><span class="p">{};</span><span class="nx">b</span><span class="p">(</span><span class="s2">&quot;[data-match-height], [data-mh]&quot;</span><span class="p">).</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">(){</span><span class="kd">var</span> <span class="nx">d</span><span class="o">=</span></td>
      </tr>
      <tr>
        <td id="L9" class="blob-line-num js-line-number" data-line-number="9"></td>
        <td id="LC9" class="blob-line-code js-file-line"><span class="nx">b</span><span class="p">(</span><span class="k">this</span><span class="p">),</span><span class="nx">c</span><span class="o">=</span><span class="nx">d</span><span class="p">.</span><span class="nx">attr</span><span class="p">(</span><span class="s2">&quot;data-match-height&quot;</span><span class="p">)</span><span class="o">||</span><span class="nx">d</span><span class="p">.</span><span class="nx">attr</span><span class="p">(</span><span class="s2">&quot;data-mh&quot;</span><span class="p">);</span><span class="nx">a</span><span class="p">[</span><span class="nx">c</span><span class="p">]</span><span class="o">=</span><span class="nx">c</span> <span class="k">in</span> <span class="nx">a</span><span class="o">?</span><span class="nx">a</span><span class="p">[</span><span class="nx">c</span><span class="p">].</span><span class="nx">add</span><span class="p">(</span><span class="nx">d</span><span class="p">)</span><span class="o">:</span><span class="nx">d</span><span class="p">});</span><span class="nx">b</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="nx">a</span><span class="p">,</span><span class="kd">function</span><span class="p">(){</span><span class="k">this</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">(</span><span class="o">!</span><span class="mi">0</span><span class="p">)})};</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_groups</span><span class="o">=</span><span class="p">[];</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_throttle</span><span class="o">=</span><span class="mi">80</span><span class="p">;</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_maintainScroll</span><span class="o">=!</span><span class="mi">1</span><span class="p">;</span><span class="kd">var</span> <span class="nx">l</span><span class="o">=-</span><span class="mi">1</span><span class="p">,</span><span class="nx">k</span><span class="o">=-</span><span class="mi">1</span><span class="p">;</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_update</span><span class="o">=</span><span class="kd">function</span><span class="p">(</span><span class="nx">a</span><span class="p">){</span><span class="k">if</span><span class="p">(</span><span class="nx">a</span><span class="o">&amp;&amp;</span><span class="s2">&quot;resize&quot;</span><span class="o">===</span><span class="nx">a</span><span class="p">.</span><span class="nx">type</span><span class="p">){</span><span class="nx">a</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="nb">window</span><span class="p">).</span><span class="nx">width</span><span class="p">();</span><span class="k">if</span><span class="p">(</span><span class="nx">a</span><span class="o">===</span><span class="nx">l</span><span class="p">)</span><span class="k">return</span><span class="p">;</span><span class="nx">l</span><span class="o">=</span><span class="nx">a</span><span class="p">}</span><span class="o">-</span><span class="mi">1</span><span class="o">===</span><span class="nx">k</span><span class="o">&amp;&amp;</span><span class="p">(</span><span class="nx">k</span><span class="o">=</span><span class="nx">setTimeout</span><span class="p">(</span><span class="kd">function</span><span class="p">(){</span><span class="nx">b</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_groups</span><span class="p">,</span><span class="kd">function</span><span class="p">(){</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_apply</span><span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">elements</span><span class="p">,</span><span class="k">this</span><span class="p">.</span><span class="nx">byRow</span><span class="p">)});</span><span class="nx">k</span><span class="o">=-</span><span class="mi">1</span><span class="p">},</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_throttle</span><span class="p">))};</span></td>
      </tr>
      <tr>
        <td id="L10" class="blob-line-num js-line-number" data-line-number="10"></td>
        <td id="LC10" class="blob-line-code js-file-line"><span class="nx">b</span><span class="p">(</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_applyDataApi</span><span class="p">);</span><span class="nx">b</span><span class="p">(</span><span class="nb">window</span><span class="p">).</span><span class="nx">bind</span><span class="p">(</span><span class="s2">&quot;load resize orientationchange&quot;</span><span class="p">,</span><span class="nx">b</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">matchHeight</span><span class="p">.</span><span class="nx">_update</span><span class="p">);</span><span class="kd">var</span> <span class="nx">m</span><span class="o">=</span><span class="kd">function</span><span class="p">(</span><span class="nx">a</span><span class="p">){</span><span class="kd">var</span> <span class="nx">d</span><span class="o">=</span><span class="kc">null</span><span class="p">,</span><span class="nx">c</span><span class="o">=</span><span class="p">[];</span><span class="nx">b</span><span class="p">(</span><span class="nx">a</span><span class="p">).</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">(){</span><span class="kd">var</span> <span class="nx">a</span><span class="o">=</span><span class="nx">b</span><span class="p">(</span><span class="k">this</span><span class="p">),</span><span class="nx">e</span><span class="o">=</span><span class="nx">a</span><span class="p">.</span><span class="nx">offset</span><span class="p">().</span><span class="nx">top</span><span class="o">-</span><span class="nx">g</span><span class="p">(</span><span class="nx">a</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;margin-top&quot;</span><span class="p">)),</span><span class="nx">h</span><span class="o">=</span><span class="mi">0</span><span class="o">&lt;</span><span class="nx">c</span><span class="p">.</span><span class="nx">length</span><span class="o">?</span><span class="nx">c</span><span class="p">[</span><span class="nx">c</span><span class="p">.</span><span class="nx">length</span><span class="o">-</span><span class="mi">1</span><span class="p">]</span><span class="o">:</span><span class="kc">null</span><span class="p">;</span><span class="kc">null</span><span class="o">===</span><span class="nx">h</span><span class="o">?</span><span class="nx">c</span><span class="p">.</span><span class="nx">push</span><span class="p">(</span><span class="nx">a</span><span class="p">)</span><span class="o">:</span><span class="mi">1</span><span class="o">&gt;=</span><span class="nb">Math</span><span class="p">.</span><span class="nx">floor</span><span class="p">(</span><span class="nb">Math</span><span class="p">.</span><span class="nx">abs</span><span class="p">(</span><span class="nx">d</span><span class="o">-</span><span class="nx">e</span><span class="p">))</span><span class="o">?</span><span class="nx">c</span><span class="p">[</span><span class="nx">c</span><span class="p">.</span><span class="nx">length</span><span class="o">-</span><span class="mi">1</span><span class="p">]</span><span class="o">=</span><span class="nx">h</span><span class="p">.</span><span class="nx">add</span><span class="p">(</span><span class="nx">a</span><span class="p">)</span><span class="o">:</span><span class="nx">c</span><span class="p">.</span><span class="nx">push</span><span class="p">(</span><span class="nx">a</span><span class="p">);</span><span class="nx">d</span><span class="o">=</span><span class="nx">e</span><span class="p">});</span><span class="k">return</span> <span class="nx">c</span><span class="p">},</span><span class="nx">g</span><span class="o">=</span><span class="kd">function</span><span class="p">(</span><span class="nx">a</span><span class="p">){</span><span class="k">return</span> <span class="nb">parseFloat</span><span class="p">(</span><span class="nx">a</span><span class="p">)</span><span class="o">||</span><span class="mi">0</span><span class="p">}})(</span><span class="nx">jQuery</span><span class="p">);</span></td>
      </tr>
</table>

  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/" aria-label="Homepage">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2014 <span title="0.02394s from github-fe119-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-suggester-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents js-suggester-field" placeholder=""></textarea>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-x close js-ajax-error-dismiss" aria-label="Dismiss error"></a>
      Something went wrong with that request. Please try again.
    </div>


      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-2b4202fc62ef88e1a007a9ed05df10607b189f42.js" type="text/javascript"></script>
      <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github-3b19668a70f4dbb3bac350c46e72efdc6f7ba76f.js" type="text/javascript"></script>
      
      
        <script async src="https://www.google-analytics.com/analytics.js"></script>
  </body>
</html>

